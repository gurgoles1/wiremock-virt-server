class AddMockMapping extends HTMLElement {

    constructor() {
        super();
    }



    connectedCallback() {
        this.innerHTML = ` <form>
                    <div class="form-group">
                        <label for="path">Path</label>
                        <input type="text" class="form-control" id="path" placeholder="Enter path">
                    </div>
                    <div class="form-group">
                        <label for="replyNumber">Status</label>
                        <input type="number" class="form-control" id="status" placeholder="Enter status" data-bind="value:replyNumber">
                    </div>
                    <div class="form-group">
                        <label for="httpMethod">Http method</label>
                        <select id="httpMethod" class="form-control">
                            <option value="GET">GET</option>
                            <option value="POST">POST</option>
                            <option value="PUT">PUT</option>
                            <option value="DELETE">DELETE</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="payload">Response payload</label>
                        <textarea id="payload" class="form-control" rows="10" contenteditable="false" placeholder="Enter payload"></textarea>
                    </div>
                    <input id="createMapping" type="button" class="btn btn-primary" value="Create" />
                    <input id="prettify" type="button" class="btn btn-primary" value="Prettify" />
                </form>`;

        const pathInput = this.querySelector("#path");
        const httpMethodInput = this.querySelector("#httpMethod");
        const statusInput = this.querySelector("#status");
        const payloadInput = this.querySelector("#payload");

        this.querySelector("#createMapping").addEventListener("click", () => {
            this.postMapping(httpMethodInput.options[httpMethodInput.selectedIndex].value, pathInput.value, statusInput.value, payloadInput.value);
            pathInput.value = "";
            statusInput.value = "";
            payloadInput.value = "";
        });
        this.querySelector("#prettify").addEventListener("click", () => {
            const currentPayload = payloadInput.value;
            payloadInput.value = AddMockMapping.prettify(currentPayload);
        });
    }

    static prettify(payloadString) {
        const currentPayloadAsObj = JSON.parse(payloadString);
         return JSON.stringify(currentPayloadAsObj, undefined, 4);
    }

    postMapping(httpMethod, path, status, body) {
        const serverEndpoint = "${server_endpoint}";
        const mockMappingEvent = new CustomEvent("mockMapping", { bubbles: true});
        fetch(`/mapping`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"httpMethod" : httpMethod, "path": path, "status": status, "body": body})
        }).then(() => {
            this.dispatchEvent(mockMappingEvent);
        });

    }

}

window.customElements.define('add-mock-mapping', AddMockMapping);
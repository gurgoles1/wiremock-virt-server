class CollapseButton extends HTMLElement {

    constructor() {
        super();
        this.collapseDir = "down";
        this.collapsed = false;
    }


    connectedCallback() {
        this.idcollapse = this.getAttribute( 'idcollapse' ) || this.idcollapse;

        this.innerHTML = `<td class="listHeader">
                                    <a id="button" class="fa fa-caret-${this.collapseDir}" style="float: right; cursor: pointer" data-toggle="collapse" data-target="#${this.idcollapse}"/>
                                  </td>`;
        const button = this.querySelector("#button");
        button.addEventListener("click", () => {
            this.collapsed = !this.collapsed;
            this.collapseDir = this.collapsed === false ? "down" : "up";
            button.setAttribute("class", `fa fa-caret-${this.collapseDir}`);
        });
    }

}

window.customElements.define('collapse-button', CollapseButton);
class MappingTable extends HTMLElement{

    constructor() {
        super();
    }

    toMappingBodyData(mapping) {
        const tableRow = document.createElement("tr");
        tableRow.innerHTML = `<tr class="listHeader">
                                <td>${mapping.request.method}</td>
                                <td>${mapping.request.url}</td>
                                <td>${mapping.response.body}</td>
                                <td style="float: right"><delete-mapping-button deleteId=${mapping.id}></delete-mapping-button></td>
                             </tr>`;
        return tableRow;
    }

    updateMappings() {
        const mappingBody = this.querySelector("#mappingBody");
        fetch(`/mapping`)
            .then(response => {
                return response.json()
            })
            .then(data => {
                data.map(it => {
                    const jsonObject = JSON.parse(it)
                    return this.toMappingBodyData(jsonObject)
                }).forEach(it => mappingBody.appendChild(it));
            })
            .then((_) => {
                this.updateMappingHeader();
            });
    }

    updateMappingHeader() {
        const mappingHeader = this.querySelector("#mappingHeader");
        const mappingBody = this.querySelector("#mappingBody");
        if (!mappingBody.hasChildNodes()) {
            mappingHeader.style = "width: 100%; display: table";
        } else {
            mappingHeader.style = "";
        }
    }

    connectedCallback() {
        this.innerHTML = `<table id="mappings" class="table table-responsive table-hover table-dark">
                    <thead id="mappingHeader" style="width: 100%; display: table">
                        <tr>
                            <th>Http-method</th>
                            <th>Path</th>
                            <th>Response</th>
                            <th><collapse-button idcollapse='mappingBody'></collapse-button></th>
                        </tr>
                    </thead>
                    <tbody id="mappingBody" class="collapsed show"></tbody>
                </table>`;

        const mappingBody = this.querySelector("#mappingBody");
        this.updateMappings();
        document.addEventListener("deleteMapping",  () => {
            mappingBody.innerHTML = "";
            this.updateMappings();
        });
        document.addEventListener("mockMapping",  () => {
            mappingBody.innerHTML = "";
            this.updateMappings();
        });
    }


}

window.customElements.define('mapping-table', MappingTable);
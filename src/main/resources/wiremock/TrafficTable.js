class TrafficTable extends HTMLElement {

    constructor() {
        super();
    }

    toTrafficBodyData(traffic) {
        const tableRow = document.createElement("tr");
        tableRow.innerHTML = `<tr>
                            <td>${traffic.responseDefinition.proxyBaseUrl ? traffic.responseDefinition.proxyBaseUrl : "mock"}</td>
                            <td>${traffic.request.url}</td>
                            <td>${traffic.response.body}</td>
                          </tr>`;
        return tableRow;
    }

    connectedCallback() {

        this.innerHTML = `<table id="traffic" class="table table-responsive table-hover table-dark">
                    <thead id="trafficHeader">
                        <tr>
                            <th>Proxy</th>
                            <th>Path</th>
                            <th>Response</th>
                            <th><collapse-button idcollapse='trafficBody'></collapse-button></th>
                        </tr>
                    </thead>
                    <tbody id="trafficBody" class="collapsed show"></tbody>
                </table>`;
        const trafficBody = this.querySelector("#trafficBody");
        const trafficHeader = this.querySelector("#trafficHeader");
        fetch(`/traffic`)
            .then(response => response.json())
            .then(data => {
                data.map(it => this.toTrafficBodyData(it))
                    .forEach(it => trafficBody.appendChild(it));
            }).then( (_) => {
            const lastTrafficBody = this.querySelector("#trafficBody");
            if (!lastTrafficBody.hasChildNodes()) {
                trafficHeader.style = "width: 100%; display: table";
            } else {
                trafficHeader.style = "";
            }
        });

    }

}

window.customElements.define('traffic-table', TrafficTable);
class DeleteMappingButton extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        const customEvent = new CustomEvent("deleteMapping", { bubbles: true });
        const deleteId = this.getAttribute('deleteId') || this.deleteId;

        this.innerHTML = `<button id="deleteButton" class="btn btn-light"><a style="color: red" class="fa fa-trash"/>
                                  </button>`;

        const deleteButton = this.querySelector("#deleteButton");
        deleteButton.addEventListener('click', () => {
            const serverEndpoint = "${server_endpoint}";
            const url = `${serverEndpoint}/mapping/${deleteId}`;
            fetch(`/mapping/${deleteId}`, {
                method: 'delete'
            }).then(() => {
                this.dispatchEvent(customEvent);
            });

        });
    }

}
window.customElements.define('delete-mapping-button', DeleteMappingButton);
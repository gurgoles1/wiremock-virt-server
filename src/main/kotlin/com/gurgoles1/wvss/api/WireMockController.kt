package com.gurgoles1.wvss.api

import com.github.tomakehurst.wiremock.http.ResponseDefinition
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import com.gurgoles1.wvss.core.FileService
import com.gurgoles1.wvss.core.RecordingStatus
import com.gurgoles1.wvss.core.VirtServerService
import com.gurgoles1.wvss.core.WireMockMapping
import com.gurgoles1.wvss.core.toStubMapping
import org.springframework.core.io.ClassPathResource
import org.springframework.http.ResponseEntity
import org.springframework.util.StreamUtils
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.*


@RequestMapping
class WireMockController(private val virtServerService: VirtServerService,
                        private val fileService: FileService) {

    @ResponseBody
    @GetMapping("/recording")
    fun recordingStatus(): RecordingStatus {
        return virtServerService.getRecordingStatus()
    }

    @ResponseBody
    @PostMapping("/import/full")
    fun importMappingsFull(@RequestBody stubMappings: List<StubMapping>): ResponseEntity<Any> {
        virtServerService.importStubsExternalRepresentation(stubMappings)
        return ResponseEntity.noContent().build()
    }

    @ResponseBody
    @PostMapping("/import/small")
    fun importMappingsSmall(@RequestBody stubMappings: List<WireMockMapping>): ResponseEntity<Any> {
        virtServerService.importStubsInternalRepresentation(stubMappings)
        return ResponseEntity.noContent().build()
    }

    @ResponseBody
    @PostMapping("/recording")
    fun recordingStart(@RequestBody body: RecordingBody): RecordingStatus {
        virtServerService.setRecordingStatus(body.recordingStatus)
        return body.recordingStatus
    }

    @ResponseBody
    @GetMapping("/traffic")
    fun getTraffic(): List<ResponseDefinition> {
        return virtServerService.getTraffic()
    }

    @ResponseBody
    @GetMapping("/mapping")
    fun getMappings(): List<String> {
        return virtServerService.getAllStubMappings().map { StubMapping.buildJsonStringFor(it) }
    }

    @ResponseBody
    @DeleteMapping("/mapping/{id}")
    fun deleteMapping(@PathVariable("id") id: String): ResponseEntity<Any> {
        val uuid = UUID.fromString(id)
        virtServerService.deleteMapping(uuid)
        return ResponseEntity.noContent().build()
    }

    @ResponseBody
    @PostMapping("/mapping")
    fun addMapping(@RequestBody wireMockMappingInput: WireMockMapping): ResponseEntity<StubMapping> {
        val stubMapping = virtServerService.saveMapping(wireMockMappingInput.toStubMapping())
        return ResponseEntity.ok(stubMapping)
    }

    @ResponseBody
    @GetMapping("/url/base")
    fun getBaseAndPaths() = fileService.getBaseAndPaths()

    @ResponseBody
    @PostMapping("/url/base")
    fun addBaseAndPath(@RequestBody baseAndPath: CreateBaseAndPath) {
        fileService.addBaseAndPath(baseAndPath.toBaseAndPath())
    }

    @ResponseBody
    @PutMapping("/url/base/{name}")
    fun addPathToBaseUrl(@PathVariable("name") baseName: String,
                         @RequestBody path: String): ResponseEntity<Any> {
        return ResponseEntity.ok(fileService.addPathToBase(baseName, path))
    }

    @ResponseBody
    @DeleteMapping("/url/base/{id}")
    fun deleteBaseAndPathUrl(@PathVariable("id") id: String): ResponseEntity<Any> {
        return ResponseEntity.ok(fileService.deleteBaseAndPath(UUID.fromString(id)))
    }

    @GetMapping("")
    fun home(): ModelAndView? {
        return ModelAndView("index")
    }

}

// Not needed because should set up a ControllerAdvice
fun <T> T?.getResponseEntity(errorMessage: String): ResponseEntity<Any> {
    return if (this == null) ResponseEntity.ok(this) else ResponseEntity.badRequest().body(errorMessage)
}

package com.gurgoles1.wvss.api

import com.gurgoles1.wvss.core.RecordingStatus

data class RecordingBody(val recordingStatus: RecordingStatus)
package com.gurgoles1.wvss.api

import com.gurgoles1.wvss.core.BaseAndPath
import java.util.*

data class CreateBaseAndPath(val baseName: String, val paths: Set<String>) {

    fun toBaseAndPath() = BaseAndPath(UUID.randomUUID(), baseName, paths)


}
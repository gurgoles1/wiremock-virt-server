package com.gurgoles1.wvss.core

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.http.RequestMethod
import com.github.tomakehurst.wiremock.matching.EqualToPattern
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder
import com.github.tomakehurst.wiremock.matching.UrlPattern
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import wiremock.com.fasterxml.jackson.annotation.JsonProperty
import wiremock.com.fasterxml.jackson.databind.annotation.JsonDeserialize

data class WireMockMapping(
    @JsonProperty("httpMethod") val httpMethod: String,
    @JsonProperty("path") val path: String,
    @JsonProperty("status") val status: Int,
    @JsonProperty("contentType") val contentType: String = "application/json",
    @JsonDeserialize(using = JsonStringDeserializer::class) @JsonProperty("body") val body: String
)

fun WireMockMapping.toStubMapping(): StubMapping {
    val responseDefinition = ResponseDefinitionBuilder
        .responseDefinition()
        .withHeader("Content-Type", "application/json")
        .withBody(body)
        .withStatus(status)
        .build()
    responseDefinition.originalRequest = OriginalRequestMock()
    val build = RequestPatternBuilder
        .newRequestPattern(RequestMethod.fromString(httpMethod), UrlPattern(EqualToPattern(path), false)) // .withUrl(wireMockMappingInput.getPath())
        .build()

    return StubMapping(
        build,
        responseDefinition
    )
}

package com.gurgoles1.wvss.core

import com.github.tomakehurst.wiremock.http.ContentTypeHeader
import com.github.tomakehurst.wiremock.http.Cookie
import com.github.tomakehurst.wiremock.http.HttpHeader
import com.github.tomakehurst.wiremock.http.HttpHeaders
import com.github.tomakehurst.wiremock.http.QueryParameter
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.RequestMethod
import wiremock.com.google.common.base.Optional

class OriginalRequestMock : Request {

    override fun getUrl(): String {
        return ""
    }

    override fun getAbsoluteUrl(): String? {
        return null
    }

    override fun getMethod(): RequestMethod? {
        return null
    }

    override fun getScheme(): String? {
        return null
    }

    override fun getHost(): String? {
        return null
    }

    override fun getPort(): Int {
        return 0
    }

    override fun getClientIp(): String? {
        return null
    }

    override fun getHeader(s: String?): String? {
        return null
    }

    override fun header(s: String?): HttpHeader? {
        return null
    }

    override fun contentTypeHeader(): ContentTypeHeader? {
        return null
    }

    override fun getHeaders(): HttpHeaders? {
        return null
    }

    override fun containsHeader(s: String?): Boolean {
        return false
    }

    override fun getAllHeaderKeys(): Set<String>? {
        return null
    }

    override fun getCookies(): Map<String, Cookie> {
        return emptyMap()
    }

    override fun queryParameter(s: String?): QueryParameter? {
        return null
    }

    override fun getBody(): ByteArray {
        return ByteArray(0)
    }

    override fun getBodyAsString(): String? {
        return null
    }

    override fun getBodyAsBase64(): String? {
        return null
    }

    override fun isMultipart(): Boolean {
        return false
    }

    override fun getParts(): Collection<Request.Part> {
        return emptyList()
    }

    override fun getPart(s: String?): Request.Part? {
        return null
    }

    override fun isBrowserProxyRequest(): Boolean {
        return false
    }

    override fun getOriginalRequest(): Optional<Request?>? {
        return null
    }
}

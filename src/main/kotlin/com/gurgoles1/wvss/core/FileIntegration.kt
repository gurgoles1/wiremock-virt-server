package com.gurgoles1.wvss.core

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import java.io.File

class FileIntegration(private val mappingsRoot: String,
                      private val baseAndPathRoot: String,
                      private val baseAndPathRootFile: String) {


    fun getBaseAndPathFile(): File = File("$baseAndPathRoot${baseAndPathRootFile}")

    fun getMappingsDirectory(): File = File(mappingsRoot)

    fun createMappingsRootFile(stubMapping: StubMapping) = File("$mappingsRoot/${stubMapping.id}_mapping.json").writeText(StubMapping.buildJsonStringFor(stubMapping))

}
package com.gurgoles1.wvss.core

import java.util.*

data class BaseAndPath(val id: UUID, val baseName: String, val paths: Set<String>) {

    fun updatePaths(paths: Set<String>) = BaseAndPath(this.id, this.baseName, this.paths.plus(paths))

}
package com.gurgoles1.wvss.core

import com.fasterxml.jackson.core.type.TypeReference
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.common.FileSource
import com.github.tomakehurst.wiremock.extension.Parameters
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.ResponseDefinition
import java.net.MalformedURLException
import java.net.URL

class WireMockTransformer(private val fileService: FileService) : ResponseDefinitionTransformer() {
    override fun getName() = "wiremock-virt-server"

    override fun transform(request: Request,
                           response: ResponseDefinition,
                           filesource: FileSource,
                           parameters: Parameters): ResponseDefinition? {

        // Om response finns retunera det
        // Om inte finns se om det är uppsatt för proxy, i sådana fall proxya request
        // Om inte finns och inte matchar någon proxy, retunera ett felmeddelande

        if (response.wasConfigured()) {
            return response
        }

        val requestingPath = request.url

        val baseUrl = fileService.getBaseAndPaths().firstNotNullOfOrNull { if (it.paths.anyContains(requestingPath)) it.baseName else null }
        if (baseUrl != null) {
            return response.getProxiedResponse(baseUrl)
        }

        return noMockOrProxyResponse()

    }

    private fun noMockOrProxyResponse() = ResponseDefinitionBuilder
        .responseDefinition()
        .withStatus(404)
        .withBody("Could not find mock or proxy response")
        .build()

    private fun Iterable<String>.anyContains(value: String) = this.any { value.contains(it) }

    private fun ResponseDefinition.getProxiedResponse(proxiedUrl: String) =
        ResponseDefinitionBuilder
            .like(this)
            .proxiedFrom(proxiedUrl)
            .withHeader("Content-type", "application/json")
            .build()

}

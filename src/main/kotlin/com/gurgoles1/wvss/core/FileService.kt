package com.gurgoles1.wvss.core

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

private val typeReference = object : TypeReference<List<BaseAndPath>>(){}

class FileService(val fileIntegration: FileIntegration,
                  val objectMapper: ObjectMapper) {

    fun addBaseAndPath(baseAndPath: BaseAndPath) {
        val file = fileIntegration.getBaseAndPathFile()
        val baseAndPaths = objectMapper.readValue(file, typeReference)
        val existingBaseAndPath = baseAndPaths.find { it.baseName == baseAndPath.baseName }

        if (existingBaseAndPath == null) baseAndPaths.plus(baseAndPath) else baseAndPaths.minusElement(existingBaseAndPath).plusElement(existingBaseAndPath.updatePaths(baseAndPath.paths))
        objectMapper.writeValue(file, baseAndPaths)
    }

    fun getBaseAndPaths(): List<BaseAndPath> {
        val file = fileIntegration.getBaseAndPathFile()
        return objectMapper.readValue(file, typeReference)
    }

    fun getBaseAndPath(baseUrl: String): BaseAndPath? {
        val file = fileIntegration.getBaseAndPathFile()
        return objectMapper.readValue(file, typeReference).find { it.baseName == baseUrl }
    }

    fun addPathToBase(baseUrl: String,
                      path: String): BaseAndPath? {
        val file = fileIntegration.getBaseAndPathFile()
        val baseAndPaths = objectMapper.readValue(file, typeReference)
        val finding = baseAndPaths.find { it.baseName == baseUrl }
        finding?.let { baseAndPaths.minusElement(it).plusElement(it.updatePaths(setOf(path))) }
        return finding
    }

    fun deleteBaseAndPath(baseAndPathId: UUID): Unit {
        val file = fileIntegration.getBaseAndPathFile()
        val filteredBaseAndPathList = objectMapper.readValue(file, typeReference).filter { it.id != baseAndPathId }
        objectMapper.writeValue(file, filteredBaseAndPathList)
    }

}
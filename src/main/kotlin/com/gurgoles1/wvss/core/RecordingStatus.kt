package com.gurgoles1.wvss.core

enum class RecordingStatus {

    ACTIVE,
    NOT_ACTIVE

}
package com.gurgoles1.wvss.core

import wiremock.com.fasterxml.jackson.core.JsonParser
import wiremock.com.fasterxml.jackson.databind.DeserializationContext
import wiremock.com.fasterxml.jackson.databind.JsonDeserializer
import wiremock.com.fasterxml.jackson.databind.JsonNode
import wiremock.com.fasterxml.jackson.databind.ObjectMapper

class JsonStringDeserializer : JsonDeserializer<String>() {

    override fun deserialize(jsonParser: JsonParser, ctxt: DeserializationContext?): String? {
        val mapper = jsonParser.codec as ObjectMapper
        val node = mapper.readTree<JsonNode>(jsonParser)
        return node.asText()
    }
}

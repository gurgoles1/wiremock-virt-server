package com.gurgoles1.wvss.core

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.Response
import com.github.tomakehurst.wiremock.matching.EqualToPattern
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder
import com.github.tomakehurst.wiremock.matching.UrlPattern
import com.github.tomakehurst.wiremock.stubbing.StubMapping


data class RequestResponse(val request: Request,
                           val response: Response) {

    fun toStubMapping(): StubMapping {
        val stubMapping = StubMapping()
        val responseDefinitionBuilder = ResponseDefinitionBuilder()
        responseDefinitionBuilder.withStatus(response.status)
        responseDefinitionBuilder.withBody(response.bodyAsString)
        val contentTypeHeader = response.headers.contentTypeHeader
        responseDefinitionBuilder.withHeader(contentTypeHeader.key(), *contentTypeHeader.values().toTypedArray())
        responseDefinitionBuilder.proxiedFrom(request.url)
        val requestPattern = RequestPatternBuilder
            .newRequestPattern(request.method, UrlPattern(EqualToPattern(request.url), false))
            .build()
        stubMapping.request = requestPattern
        val responseDefinition = responseDefinitionBuilder.build()
        responseDefinition.originalRequest = OriginalRequestMock()
        stubMapping.response = responseDefinition
        return stubMapping
    }

}
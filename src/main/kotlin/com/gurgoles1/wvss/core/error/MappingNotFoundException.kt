package com.gurgoles1.wvss.core.error

class MappingNotFoundException(message: String): Exception()
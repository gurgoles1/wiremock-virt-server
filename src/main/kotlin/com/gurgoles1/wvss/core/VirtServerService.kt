package com.gurgoles1.wvss.core

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.Response
import com.github.tomakehurst.wiremock.http.ResponseDefinition
import com.github.tomakehurst.wiremock.stubbing.StubImport
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import com.gurgoles1.wvss.core.error.MappingNotFoundException
import mu.KotlinLogging
import java.io.File
import java.lang.Exception
import java.util.*

private val logger = KotlinLogging.logger {}

class VirtServerService(private val wireMockServer: WireMockServer,
                        private val fileIntegration: FileIntegration) {

    // Maybe refactor to state pattern or something
    private var currentRecordingStatus = RecordingStatus.NOT_ACTIVE

    fun saveMapping(stubMapping: StubMapping): StubMapping {
        wireMockServer.addStubMapping(stubMapping)
        fileIntegration.createMappingsRootFile(stubMapping)
        return stubMapping
    }

    fun loadMappings(): List<StubMapping> {
        val directory = fileIntegration.getMappingsDirectory()
        val listFiles: Array<File> = directory.listFiles() ?: emptyArray()
        val stubMappings = listFiles.mapNotNull { it.buildToStubMappingJson() }
        wireMockServer.importStubs(
            StubImport(
                stubMappings,
                StubImport.Options(StubImport.Options.DuplicatePolicy.IGNORE, false)
            )
        )
        return stubMappings
    }

    fun setRecordingStatus(recordingStatus: RecordingStatus) {
        currentRecordingStatus = recordingStatus
    }

    fun getRecordingStatus() = currentRecordingStatus

    fun addRecording(request: Request, response: Response) {
        if (response.status == 200 && currentRecordingStatus == RecordingStatus.ACTIVE) {
            val recording = RequestResponse(request, response)
            wireMockServer.addStubMapping(recording.toStubMapping())
        }
    }

    fun getTraffic(): List<ResponseDefinition> {
        return wireMockServer
            .allServeEvents
            .map { it.responseDefinition }
    }

    fun importStubsInternalRepresentation(wireMockMappings: List<WireMockMapping>) {
        wireMockServer.importStubs(StubImport(wireMockMappings.map { it.toStubMapping() }, StubImport.Options(StubImport.Options.DuplicatePolicy.IGNORE, false)))
    }

    fun importStubsExternalRepresentation(stubMappings: List<StubMapping>) {
        wireMockServer.importStubs(StubImport(stubMappings, StubImport.Options(StubImport.Options.DuplicatePolicy.IGNORE, false)))
    }

    fun deleteMapping(uuid: UUID) {
        // Need to remove file from classpath also (probably not)
        val mapping = wireMockServer.stubMappings
            .find { it.id == uuid } ?: throw MappingNotFoundException("Could not find mapping with id $uuid")
        mapping.let { wireMockServer.removeStubMapping(it) }
    }

    fun getAllStubMappings(): List<StubMapping> = wireMockServer.stubMappings

    fun addRequestResponseListener() {
        wireMockServer.addMockServiceRequestListener { request: Request, response: Response -> addRecording(request, response) }
    }

    private fun File.buildToStubMappingJson(): StubMapping? {
        return try {
            StubMapping.buildFrom(this.readText())
        } catch (exception: Exception) {
            logger.info { this.name }
            null
        }
    }
}
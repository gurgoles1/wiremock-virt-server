package com.gurgoles1.wvss.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.gurgoles1.wvss.api.WireMockController
import com.gurgoles1.wvss.core.FileIntegration
import com.gurgoles1.wvss.core.FileService
import com.gurgoles1.wvss.core.WireMockTransformer
import com.gurgoles1.wvss.core.VirtServerService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import kotlin.collections.HashMap

@Configuration
class WireMockConfiguration {

    private val logger = KotlinLogging.logger {}

    @Bean
    fun wireMockServer(wireMockTransformer: WireMockTransformer,
                       @Value("\${wireMock.port}") wireMockPort: Int?): WireMockServer {

        val extensions = WireMockConfiguration.wireMockConfig()
            .extensions(wireMockTransformer)
            .let { setWireMockPort(wireMockPort, it) }
        val wireMockServer = WireMockServer(extensions)
        wireMockServer.start()

        return wireMockServer
    }

    private fun setWireMockPort(wireMockPort: Int?, it: WireMockConfiguration): WireMockConfiguration {
        return if (wireMockPort == null) {
            val dynamicPort = it.dynamicPort()
            logger.info { "Wiremock started at port $dynamicPort" }
            return dynamicPort
        } else {
            it.port(wireMockPort.toInt())
        }
    }

    @Bean
    fun virtServerService(
        wireMockServer: WireMockServer,
        fileIntegration: FileIntegration
    ) = VirtServerService(wireMockServer, fileIntegration)

    @Bean
    fun fileIntegration(@Value("\${wireMock.files.mappings.directory}") mappingsRoot: String,
                        @Value("\${wireMock.files.baseAndPath.directory}") baseAndPathRoot: String,
                        @Value("\${wireMock.files.baseAndPath.rootFileName}") baseAndPathRootFile: String) = FileIntegration(mappingsRoot, baseAndPathRoot, baseAndPathRootFile)

    /*@Bean
    public WireMockConfigurationCustomizer customizer(final WireMockTransformer wireMockTransformer) {
        return config -> {
            config.extensions(wireMockTransformer);
            config.port(8089);
            config.enableBrowserProxying(true);
        };
    }*/

    /*@Bean
    public WireMockConfigurationCustomizer customizer(final WireMockTransformer wireMockTransformer) {
        return config -> {
            config.extensions(wireMockTransformer);
            config.port(8089);
            config.enableBrowserProxying(true);
        };
    }*/
    @Bean
    fun wireMockTransformer(fileService: FileService): WireMockTransformer {
        return WireMockTransformer(fileService)
    }

    @Bean
    fun wireMockController(virtServerService: VirtServerService, fileService: FileService): WireMockController {
        return WireMockController(virtServerService, fileService)
    }

    @Bean // Its enough to just add a jackson module as bean to register it to an objectmapper
    fun objectMapper(): KotlinModule {
        return KotlinModule()
    }

    @Bean
    fun fileService(fileIntegration: FileIntegration,
                    objectMapper: ObjectMapper): FileService {
        return FileService(fileIntegration, objectMapper)
    }


}

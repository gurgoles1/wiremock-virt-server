package com.gurgoles1.wvss.config

import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@Profile("dev")
class DevelopmentFilter: Filter{
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val httpServletResponse = response as HttpServletResponse
        val httpServletRequest = request as HttpServletRequest

        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*")
        httpServletResponse.addHeader("Access-Control-Allow-Headers", "*")
        httpServletResponse.addHeader("Access-Control-Allow-Methods", "*")
        chain.doFilter(httpServletRequest, httpServletResponse)
    }


}
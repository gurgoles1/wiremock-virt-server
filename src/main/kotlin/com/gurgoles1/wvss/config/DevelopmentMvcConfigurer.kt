package com.gurgoles1.wvss.config

import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

//@Component
@Profile("dev")
class DevelopmentMvcConfigurer: WebMvcConfigurer {

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/*").allowedOrigins("http://localhost:3000")
    }
}
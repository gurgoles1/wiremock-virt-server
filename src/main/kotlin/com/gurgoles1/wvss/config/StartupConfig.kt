package com.gurgoles1.wvss.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.Response
import com.gurgoles1.wvss.core.BaseAndPath
import com.gurgoles1.wvss.core.FileService
import com.gurgoles1.wvss.core.VirtServerService
import com.gurgoles1.wvss.core.WireMockMapping
import com.gurgoles1.wvss.core.toStubMapping
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.io.File
import java.util.*

private val logger = KotlinLogging.logger {}

@Component
class StartupConfig(private val virtServerService: VirtServerService,
                    private val objectMapper: ObjectMapper,
                    @Value("\${wireMock.files.mappings.directory}") private val mappingsRoot: String,
                    @Value("\${wireMock.files.baseAndPath.directory}") private val baseAndPathRoot: String,
                    @Value("\${wireMock.files.baseAndPath.rootFileName}") private val baseAndPathRootFile: String){

    @EventListener
    fun loadMappingsFromSource(event: ContextRefreshedEvent) {
        virtServerService.loadMappings()
        initDirectoriesAndFiles()
        virtServerService.addRequestResponseListener()
    }

    fun initDirectoriesAndFiles() {
        val baseAndPathRootExist = File(baseAndPathRoot).createDirectoryIfNotExist()
        File(mappingsRoot).createDirectoryIfNotExist()
        if (baseAndPathRootExist) File("$baseAndPathRoot$baseAndPathRootFile").fillWithEmptyArray(objectMapper).createNewFile()
    }


    /* fun addDummyMock() {
        wireMockServer.addStubMapping(WireMockMapping(
            httpMethod = "GET",
            path = "/public/v1/posts",
            status = 200,
            body = """
              [
                {
                    "id": 1,
                    "body": "some comment",
                    "postId": 1
                },
                {
                    "id": 2,
                    "body": "some comment",
                    "postId": 1
                }
             ]  
            """
        ).toStubMapping())
    } */

    private fun File.createDirectoryIfNotExist(): Boolean {
        return if (!this.exists()) {
            val wasCreated = this.mkdir()
            if (wasCreated) {
                logger.info { "${this.absolutePath} directory was created" }
                true

            } else {
                logger.info { "${this.absolutePath} directory could not be created" }
                false
            }
        } else {
            logger.info { "${this.absolutePath} directory already exist" }
            true
        }
    }

}

private fun File.fillWithEmptyArray(objectMapper: ObjectMapper): File {
    val emptyList = emptyArray<Any>()
    objectMapper.writeValue(this, emptyList)
    return this
}

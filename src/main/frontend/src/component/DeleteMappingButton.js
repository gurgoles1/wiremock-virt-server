import {FaTrash} from "react-icons/fa";
import {Button} from "react-bootstrap";

function DeleteMappingButton(props) {

    return <Button onClick={() => props.deleteMappingFunction(props.mappingId)}>
        <FaTrash/>
    </Button>

}

export default DeleteMappingButton;
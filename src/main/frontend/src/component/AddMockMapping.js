import {Button, Form} from "react-bootstrap";
import {useState} from "react";

function AddMockMapping(props) {

    const initialState = {
        path: "",
        status: "",
        httpMethod: "",
        responsePayload: ""
    };
    const [state, setState] = useState(initialState)

    function handleMockMapping(event) {
        const form = event.currentTarget;
        event.preventDefault();
        event.stopPropagation()

        const path = form[0].value;
        const status = form[1].value;
        const method = form[2].value;
        const body = form[3].value;

        props.addMockMapping(path, status, method, body)
            .then(() => {
                setState(initialState)
            })
    }


    return <Form onSubmit={handleMockMapping}>
        <Form.Group>
            <Form.Label>Path</Form.Label>
            <Form.Control type="text" placeholder="Enter path" value={state.path} onChange={(event) => setState({path: event.target.value})}/>
        </Form.Group>
        <Form.Group>
            <Form.Label>Status</Form.Label>
            <Form.Control type="text" placeholder="Enter status" value={state.status} onChange={(event) => setState({status: event.target.value})}/>
        </Form.Group>
        <Form.Group>
            <Form.Label>Http Method</Form.Label>
            <Form.Select value={state.httpMethod} onChange={(event) => setState({httpMethod: event.target.value})}>
                <option value="GET">GET</option>
                <option value="POST">POST</option>
                <option value="PUT">PUT</option>
                <option value="DELETE">DELETE</option>
            </Form.Select>
        </Form.Group>
        <Form.Group>
            <Form.Label>Response payload</Form.Label>
            <Form.Control as="textarea" rows={8} onChange={(event) => setState({responsePayload: event.target.value})} value={state.responsePayload}/>
        </Form.Group>
        <Button variant="primary" type="submit">Create Mapping</Button> {' '}
        <Button onClick={() => {
            const parsedJson = JSON.parse(state.responsePayload)
            setState({responsePayload: JSON.stringify(parsedJson, undefined, 4)})
        }}>Prettify</Button> {' '}
    </Form>

}


export default AddMockMapping;
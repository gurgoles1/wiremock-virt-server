import {Button, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import ApiConfEntry from "./ApiConfEntry";
import ApiConfEditEntry from "./ApiConfEditEntry";

const ApiConf = () => {

    const [state, setState] = useState([])
    const [edits, setEdits] = useState([])
    const [added, setSaveOrDelete] = useState([])
    useEffect(() => {
        fetch("http://localhost:8080/url/base", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            return response.json()
        }).then((baseAndPaths) => {
            console.log(baseAndPaths)
            setState(baseAndPaths)
        });

    }, [added])

    function addBaseAndPath() {
        setEdits(edits.concat(edits.length + 1))
    }

    const addConfEntry = (id, base, paths) => {
        console.log("Api-conf ", base, paths)
        fetch("http://localhost:8080/url/base", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({baseName: base, paths: paths})
        }).then(() => {
            setSaveOrDelete(added.concat("saved"))
            setEdits(edits.filter(it => it !== id))
        })
    }

    const deleteApiConf = (id) => {
        console.log(id)
        fetch(`http://localhost:8080/url/base/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            setSaveOrDelete(added.concat("deleted"))
        })
    }

    const cancelConfEntry = (id) => {
        setEdits(edits.filter(it => it !== id))
    }

    return <Table variant="dark">
        <thead id="mappingHeader">
        <tr>
            <th className="col-md-3">Base</th>
            <th className="col-md-6">Paths</th>
            <th className="col-md-3">
                <Button onClick={addBaseAndPath}>Add base and path</Button>
            </th>
        </tr>
        </thead>
        <tbody className="body">
        {state.map(data => <ApiConfEntry key={data.id} data={data} deleteApiConf={deleteApiConf}/>)}
        {edits.map((it) => <ApiConfEditEntry key={it} id={it} addConfEntry={addConfEntry} cancelConfEntry={cancelConfEntry}/>)}
        </tbody>
    </Table>


}

export default ApiConf
function TrafficEntry(props) {
    return <tr>
        <td>{props.proxyBaseUrl ? props.proxyBaseUrl : "mock"}</td>
        <td>{props.url}</td>
        <td>{props.body}</td>
    </tr>
}

export default TrafficEntry;


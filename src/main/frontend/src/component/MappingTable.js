import {Button, Table} from "react-bootstrap";
import {useState} from "react";
import MappingEntry from "./MappingEntry";
import Collapse from "react-bootstrap/Collapse";



function MappingTable(props) {

    const [open, setOpen] = useState(true)

    return <Table variant="dark">
        <thead id="mappingHeader">
        <tr>
            <th className="col-md-3">Http-method</th>
            <th className="col-md-3">Path</th>
            <th className="col-md-3">Response</th>
            <th className="col-md-3">
                <Button onClick={() => {
                    setOpen(!open)
                }}>Collapse</Button>
            </th>
        </tr>
        </thead>
        <Collapse in={open}>
            <tbody className="body">
                {props.mappings.map(data => <MappingEntry key={data.id} mappingId={data.id} deleteMappingFunction={props.deleteMapping} method={data.request.method} url={data.request.url} body={data.response.body} />)}
            </tbody>
        </Collapse>
    </Table>
}


export default MappingTable;
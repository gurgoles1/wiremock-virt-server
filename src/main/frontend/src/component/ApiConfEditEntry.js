import {Button, FormControl, InputGroup} from "react-bootstrap";
import {useState} from "react";
import AddConfEditEntryPaths from "./AddConfEditEntryPaths";

const ApiConfEditEntry = (props) => {

    const [addPaths, setAddPaths] = useState([1]);
    const [editPaths, setEditPaths] = useState([]);
    const [baseUrl, setBaseUrl] = useState();

    function addPath(id) {
        setAddPaths(addPaths.concat(id + 1))
    }

    function editPath(id, value) {
        const updatedEditPaths = editPaths.filter((it) => it.id === id)
        setEditPaths(updatedEditPaths.concat({id: id + 1, value: value}))
    }

    function deletePath(id) {
        if (addPaths.length >= 1) {
            setAddPaths(addPaths.filter((it) => it !== id))
        }
    }

    return <tr>
        <td>
            <InputGroup>
                <FormControl type="text" placeholder="Enter base url" aria-label="Base" onChange={(it) => setBaseUrl(it.target.value)} />
            </InputGroup>
        </td>
        <td>
            {addPaths.map((it) => <AddConfEditEntryPaths key={it} id={it} addPath={addPath} editPath={editPath} deletePath={deletePath}/>)}
        </td>
        <td>
            <Button onClick={() => props.addConfEntry(props.id, baseUrl, editPaths.map(it => it.value))}>Save</Button> {' '}
            <Button onClick={() => props.cancelConfEntry(props.id)}>Cancel</Button>
        </td>
    </tr>

}

export default ApiConfEditEntry
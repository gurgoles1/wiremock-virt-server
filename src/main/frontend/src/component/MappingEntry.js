import DeleteMappingButton from "./DeleteMappingButton";

function MappingEntry(props) {

    return <tr>
        <td>{props.method}</td>
        <td>{props.url}</td>
        <td>{props.body}</td>
        <td><DeleteMappingButton mappingId={props.mappingId} deleteMappingFunction={props.deleteMappingFunction}/></td>
    </tr>

}

export default MappingEntry
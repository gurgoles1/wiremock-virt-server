import {Button} from "react-bootstrap";
// Lägg till så går att editera
const ApiConfEntry = (props) => {

    return <tr>
        <td>{props.data.baseName}</td>
        <td>{props.data.paths.join(",")}</td>
        <td>
            <Button>Edit</Button> {' '}
            <Button onClick={() => props.deleteApiConf(props.data.id)}>Delete</Button>
        </td>
    </tr>

}

export default ApiConfEntry
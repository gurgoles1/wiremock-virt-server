import {Button, FormControl, InputGroup} from "react-bootstrap";
import {FaMinus, FaPlus} from "react-icons/all";

const AddConfEditEntryPaths = (props) => {

    return <>
        <InputGroup>
            <FormControl type="text" placeholder="Enter path" aria-label="Paths" onChange={(it) => props.editPath(props.id, it.target.value)}/>
            <Button onClick={() => props.addPath(props.id)}>
                <FaPlus/>
            </Button>
            <Button onClick={() => props.deletePath(props.id)}>
                <FaMinus/>
            </Button>
        </InputGroup>
    </>
}

export default AddConfEditEntryPaths
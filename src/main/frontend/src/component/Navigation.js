import {Container, Nav, Navbar} from "react-bootstrap";

const Navigation = () => {


    return <>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#">Virt Editor</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="#traffic">Traffic</Nav.Link>
                        <Nav.Link href="#apiconf">Api Config</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    </>


}

export default Navigation
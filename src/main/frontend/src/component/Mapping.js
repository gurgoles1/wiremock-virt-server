import AddMockMapping from "./AddMockMapping";
import MappingTable from "./MappingTable";
import {useEffect, useState} from "react";

function Mapping() {

    const [deleteId, setDeleted] = useState()
    const [addMockId, setAddMock] = useState()
    const [mappings, setMappings] = useState([]);

    const addMockMapping = (path, status, method, body) => fetch(`http://localhost:8080/mapping`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({"path": path, "status": status, "httpMethod" : method, "body": body})
    }).then((response) => {
        return response.json()
    }).then((addMockMapping) => {
        setAddMock(addMockMapping.id)
    });

    const deleteMapping = (mappingId) => {
        fetch(`http://localhost:8080/mapping/${mappingId}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            setDeleted(mappingId)
        })
    }

    useEffect(() => {
        fetch("http://localhost:8080/mapping").then(
            response => {
                return response.json()
            }
        ).then(response => {
            const jsonResponseArray = response.map(it => JSON.parse(it))
            setMappings(jsonResponseArray)
        })
    }, [deleteId, addMockId])

    return <>
        <AddMockMapping addMockMapping={addMockMapping}/>
        <MappingTable mappings={mappings} deleteMapping={deleteMapping}/>
    </>

}

export default Mapping
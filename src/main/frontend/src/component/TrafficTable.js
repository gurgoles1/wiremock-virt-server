import {Button, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import TrafficEntry from "./TrafficEntry";
import Collapse from "react-bootstrap/Collapse";

function TrafficTable() {

    const [state, setState] = useState([])

    // Array makes it only run ones
    useEffect(() => {
        fetch("http://localhost:8080/traffic")
            .then(response => response.json())
            .then(response => setState(response))
    }, [])

    return <Table striped variant="dark">
        <thead id="trafficHeader">
        <tr>
            <th className="col-md-3">Proxy</th>
            <th className="col-md-3">Path</th>
            <th className="col-md-3">Response</th>
            <th className="col-md-3">
                <Button>Collapse</Button>
            </th>
        </tr>
        </thead>
        {
        <Collapse>
            <tbody>
                {state.map(it => <TrafficEntry url={it.url} body={it.body} proxyBaseUrl={it.proxyBaseUrl}/>)}
            </tbody>
        </Collapse>
        }
    </Table>

}

export default TrafficTable;
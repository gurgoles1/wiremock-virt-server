import './App.css';
import {Container, Row} from 'react-bootstrap';
import TrafficTable from "./component/TrafficTable";
import Mapping from "./component/Mapping";
import Navigation from "./component/Navigation";
import {HashRouter, Route, Switch} from "react-router-dom";
import ApiConf from "./component/ApiConf";

// Centrera Virt Editor med text-align=center
// Fixa spacing mellan Row med Mock mapping och Tabellerna

function App() {
  return (
    <div>
        <Navigation/>
        <Container>
            <Row className="justify-content-md-center">
                <HashRouter>
                    <Switch>
                        <Route exact path="/" component={Mapping}/>
                        <Route exact path="/apiconf" component={ApiConf}/>
                        <Route exact path="/traffic" component={TrafficTable}/>
                    </Switch>
                </HashRouter>
            </Row>
        </Container>
    </div>
  );
}

export default App;

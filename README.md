#Wiremock virt server
This application is a try, with the help of wiremock, to set up a simple
virt server that can be used to mock request in a simple way and also to proxy to the desired source if mock does not exist.
The components that exists is a simple ui and api to set up mocks and
configure the apis to proxy requests to and also the wiremock server that is getting
configured. It's in that way possible to configure wiremock with it's own api.

## Mapping
Mapping is the core of the applications where you set up the mappings that will
be returned if a proxy matching the mapping is coming to the wiremock server.

## Api conf
Api configuration is the way to set up what incoming requests to wiremock should be redirected
to what url in case of a mapping does not exist. With this approach it's possible to integrate to the 
the wiremock server and still have your application function as usual but with the extra layer
that you can set up mocks for the requests u like. 

## Installation
TODO